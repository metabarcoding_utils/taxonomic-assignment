#!/usr/bin/env nextflow
/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    taxonomic assignment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    GitLab : https://gitlab.com/metabarcoding_utils/taxonomic-assignment
----------------------------------------------------------------------------------------
*/

process VSEARCH_HITS {

    label 'process_single'

    input:
    path fasta_seq
    path refdb

    output:
    path "vsearch_hits.tsv"

    script:
    if( params.best_hit_only)

        """
        vsearch --usearch_global ${fasta_seq} \
                --db ${refdb} \
                --id ${params.vsearch_unclassified_threshold} \
                --maxaccepts 0 \
                --top_hits_only \
                --query_cov ${params.vsearch_query_cov} \
                --userout vsearch_hits.tsv \
                --threads ${task.cpus} \
                --iddef ${params.vsearch_iddef}  \
                --userfields "query+id+target+qcov"
        """

    else

        """
        vsearch --usearch_global ${fasta_seq} \
                --db ${refdb} \
                --id ${params.vsearch_unclassified_threshold} \
                --maxaccepts ${params.vsearch_max_accepts} \
                --query_cov ${params.vsearch_query_cov} \
                --userout vsearch_hits.tsv \
                --threads ${task.cpus} \
                --iddef ${params.vsearch_iddef} \
                --userfields "query+id+target+qcov"
        """
}

process GET_LCA {

    label 'process_single'

    input:
    path vsearch_hits
    path refdb_taxo

    output:
    path "lca_out.tsv"

    script:
    if(params.best_hit_only)
        lca_threshold = 100
    else
        lca_threshold = params.vsearch_lca_threshold

    """
    export LC_NUMERIC="C"
    
    awk '
        BEGIN{

            FS = "\t"
            OFS = "\t"
            print "asv_id","best_hit_score", "lower_threshold_score", "min_query_coverage", "max_query_coverage", "references", "lca_taxonomy"
        }

        NR == FNR {

            taxodict[\$1] = \$2
            next

        }

        
        \$1 != amplicon {

            if (FNR > 1){
                output = sep = ""
                for (i=1; i<=n; i++) {
                    output = output sep taxo[i]
                    sep = ";"
                }
                print amplicon, maxscore, minscore, minqcov, maxqcov, refs, output
            }

            amplicon = \$1
            maxscore = \$2
            minscore = \$2 * ${lca_threshold} / 100
            refs = \$3
            n = split(taxodict[\$3], taxo, ";")
            minqcov = \$4
            maxqcov = \$4
            next
        }

        \$2 >= minscore {

            minqcov = (minqcov < \$4) ? minqcov : \$4
            maxqcov = (maxqcov > \$4) ? maxqcov : \$4
            refs = refs","\$3
            split(taxodict[\$3], tmp, ";")

            for (i=1;i<=n;i++) {

                taxo[i] = (taxo[i] == tmp[i]) ? taxo[i] : "*"

            }
        }

        END{

            output = sep = ""

            for (i=1;i<=n;i++) {

                output = output sep taxo[i]
                sep = ";"

            }

            print amplicon,maxscore,minscore, minqcov, maxqcov, refs, output

        } 
    ' ${refdb_taxo} ${vsearch_hits}  > "lca_out.tsv"

    """

}

process ASSIGN_IDTAXA {

    label 'process_medium'

    input:
    path(fasta_seq)
    path(refdb)

    output:
    path("idtaxa_out.tsv")

    script:
    """
    #!/usr/bin/env Rscript

    set.seed(100)

    ##########################################
    # import sequences
    ##########################################

    sequences <- Biostrings::readDNAStringSet("${fasta_seq}", format = "fasta")

    ##########################################
    # assign with idtaxa
    ##########################################

    trainingSet <- readRDS("${refdb}")

    idtaxa_res <- DECIPHER::IdTaxa(
        sequences,
        trainingSet,
        strand = "top",
        threshold = ${params.idtaxa_threshold},
        minDescend = ${params.idtaxa_minDescend},
        processors = ${task.cpus}
    )

    taxonomy <- vapply(
        idtaxa_res,
        function(x) paste(x[["taxon"]], collapse = ";"),
        character(1)
    )

    confidence <- vapply(
        idtaxa_res,
        function(x) paste(round(x[["confidence"]], digits = 1), collapse = ";"),
        character(1)
    )


    ##########################################
    # assemble into one table
    ##########################################

    res <- data.frame(
        asv_id = names(taxonomy),
        taxonomy = sub("^Root;", "", taxonomy),
        confidence = sub("^[^;]+;", "",confidence)
        )

    write.table(
        res,
        file = "idtaxa_out.tsv",
        quote = FALSE,
        sep = "\t",
        row.names = FALSE
    )

    """
}

process ASSIGN_DADA2 {

    label 'process_medium'
    label 'process_long'

    input:
    path(fasta_seq)
    path(refdb)

    output:
    path("dada2_out.tsv")

    script:
    """
    #!/usr/bin/env Rscript
    
    library(tidyverse)

    set.seed(100)

    seq <- dada2::getSequences("${fasta_seq}")

    taxa <- dada2::assignTaxonomy(
        seq,
        "${refdb}",
        outputBootstraps = TRUE,
        minBoot = ${params.dada2_minBoot},
        taxLevels = NA,
        multithread = ${task.cpus}
    )

    boot_scores <- taxa\$boot |>
        as_tibble(.name_repair = "unique") |>
        unite("scores", everything(), sep = ";") |> 
        pull(scores)

    taxa_res <- taxa\$tax |> 
        as_tibble(.name_repair = "unique") |>
        unite("taxonomy", everything(), sep = ";") |>
        mutate(confidence = boot_scores) |> 
        rowwise() |> 
        mutate(
            confidence = str_remove(confidence, str_c("(;[^;]+){", str_count(taxonomy,";NA"),"}\$"))
        ) |> 
        ungroup() |> 
        mutate(
            taxonomy = str_remove(taxonomy, "(;NA)+\$"),
            asv_id = names(seq)
        ) |> 
            relocate(asv_id)

    write.table(
        taxa_res,
        file = "dada2_out.tsv",
        quote = FALSE,
        sep = "\t",
        row.names = FALSE
    )
    """

}

workflow {

    Channel.fromPath(params.inputfasta)
        .splitFasta( by: params.fastachunks, file: true )
        .set{ splitted_fasta }

    if(!params.skip_vsearch) {

        if(params.best_hit_only)
            vsearch_res_name = "vsearch_best_hit.tsv"
        else
            vsearch_res_name = "vsearch_lca_${params.vsearch_lca_threshold}.tsv"

        VSEARCH_HITS(splitted_fasta, file(params.vsearch_refdb_seq))
            .set{ hits }

        GET_LCA(hits, file(params.vsearch_refdb_taxo))
            .collectFile(keepHeader: true, skip: 1, storeDir: params.outdir, name: vsearch_res_name)
    }
        
    if(!params.skip_idtaxa) {

        idtaxa_res_name = "idtaxa_res.tsv"

        ASSIGN_IDTAXA(splitted_fasta, file(params.idtaxa_refdb))
            .collectFile(keepHeader: true, skip: 1, storeDir: params.outdir, name: idtaxa_res_name)

    }

    if(!params.skip_dada2) {

    dada2_res_name = "dada2_res.tsv"

    ASSIGN_DADA2(splitted_fasta, file(params.dada2_refdb))
        .collectFile(keepHeader: true, skip: 1, storeDir: params.outdir, name: dada2_res_name)

    }


}