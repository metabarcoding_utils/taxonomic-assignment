# ASVs taxonomic assignment

## Presentation

This nextflow module taxonomically assigns ASVs with three tools, VSEARCH (best-hit approach), IDTAXA (classifier) and the dada2 function `assignTaxonomy()` (RDP classifier). By default the workflow only run VSEARCH.

## The approaches

### IDTAXA

IDTAXA is a classifier described as having a higher accuracy than popular classifiers and yields far fewer over classifications.

### `assignTaxonomy()` for the R package dada2

The function from dada2 to assign ASVs is assignTaxonomy(), it is an implementation of the RDP Naive Bayesian Classifier algorithm, a popular tool for taxonomic assignment.

### VSEARCH (best hit and least common ancestor)

Classifiers might sometimes fail to assign some ASVs. This could happen with taxonomic groups for which there are not enough sequences in the reference database. In that case, it might be interesting to know which are the closest reference sequences for those ASVs.

VSEARCH, with the command --usearch_global, compare target sequences (reference sequences) to query sequences (OTUs or ASVs) using global pairwise alignment. In our taxonomic assignment workflow, VSEARCH --usearch_global is used to assigned using the least common ancestor or best hit approach:

* **least common ancestor (LCA) approach**: assign to an ASV (or OTU), the common taxonomy between all hits (reference sequences) comprised between the best hit similarity and a given threshold called LCA threshold in our workflow. If the best hit similarity is 97% and the LCA threshold is 99% (default value), all the hits between 96.03 % (0.97*0.99) and 97% would be considered.
* **best hit approach**: the same as for the LCA approach but considering only best hits.

## Quick start

### Prerequisites

You need nextflow >= 23.04.1, VSEARCH >= 2.18.0 and R >= 4.1.1 (only for IDTAXA and dada2 approaches) installed on your system. You'll also need the R library DECIPHER installed to run IDTAXA and dada2 and dplyr to assign with dada2 function `assignTaxonomy()`. gawk need to be installed too but that is already the case for the majority of Linux distributions. If you are working on ABiMS cluster, all you need is already installed. See below for ABiMS cluster instructions.

### Generic usage

To run the current stable version:

```bash
nextflow \
  run https://gitlab.com/metabarcoding_utils/taxonomic-assignment \
  -r main \
  -params-file parameters.yaml
```

To run a specific version (v0.2.0 for example):

```bash
nextflow \
  run https://gitlab.com/metabarcoding_utils/taxonomic-assignment \
  -r v0.2.0 \
  -params-file parameters.yaml
``` 

If you change a parameter and want to rerun the workflow, use the option `-resume` to avoid having to rerun everything.

The worflow parameters are defined in `parameters.yaml` or in any other `yaml` file specified after `-params-file`. Three parameters are mandatory to run the workflow (see parameters section for more details):

* `inputfasta`
* `vsearch_refdb_seq`
* `vsearch_refdb_taxo`

Here an example of the content of `parameters.yaml`:

```yaml
# path to input file
inputfasta: 'my_fasta_file.fasta.gz'

# path to reference database sequences
vsearch_refdb_seq: 'pr2_vsearch_formated.fasta.gz'

# path to reference database taxonomy
vsearch_refdb_taxo: 'pr2_vsearch_formated_taxo.tsv'
```

Reference database files (sequences and taxonomy) can easily be generated using <https://gitlab.com/metabarcoding_utils/metab-refdb-importer>. You will find in this repository template parameters files to get EukRibo and PR2 files with the good format for taxonomic assignment.

## On ABiMS cluster

To be able to run the workflow on the cluster you first need to make nextflow available with the following command:

``` bash
module load nextflow/23.04.1
```

After that you can simply run the latest version of the workflow with the command below. Remember to lauch this command either with `srun` or in a bash script launched with `sbatch`. 

``` bash
nextflow \
  run https://gitlab.com/metabarcoding_utils/taxonomic-assignment \
  -r main \
  -profile abims_config \
  -params-file parameters.yaml
```

## Parameters

### General options

* `inputfasta`: path to fasa file you want to assign (mandatory)
* `fastachunks`: size of fasta chunks, set a value lower that the total number of sequences in `inputfasta` (default: 1000)
* `outdir`: path to directory for outputs (default: "outputs")


### LCA approach with vsearch

* `skip_vsearch`: set to `true` to skip taxonomic assignement with vsearch. If `false`,
`vsearch_refdb_seq` and `vsearch_refdb_taxo` have to be specified (default: `false`)
* `best_hit_only`: set to `true` if you want best hit approach (default: `false`)
* `vsearch_refdb_seq`: path to fasta file with reference sequences (mandatory). Headers only contain the reference sequence ids.
* `vsearch_refdb_taxo`: path to a two column tsv file with the taxonomy of reference sequences in `vsearch_refdb_seq` (mandatory). The first column contains the reference id and the second contains semicolon separated taxonomy. This table can easily be generated using https://gitlab.com/metabarcoding_utils/metab-refdb-importer
* `vsearch_unclassified_threshold`: minimum similarity score (default: 0.8)
* `vsearch_lca_threshold`: lower threshold in percentage for LCA (default: 99). The lower threshold is computed as followed: `best_score * vsearch_lca_threshold`. If the best hit similarity is for example 97 %, with default values all hits between 96.03 % (97 * 0.99) and 97 % would be considered.
* `vsearch_max_accepts`: maximum nuber of matches for LCA (default: 100). All hits are considered for best hit approach
* `vsearch_iddef`: pairwise identity definition, see vsearch documentation for more information. Accepted values are 0, 1, 2, 3 and 4. Default value is 1.
>  Change the pairwise identity definition used in --id. Values accepted are:
> 
> 0.  CD-HIT definition: (matching columns) / (shortest sequence length).
>
> 1.  edit distance: (matching columns) / (alignment length).
>
> 2.  edit distance excluding terminal gaps (same as --id).
>
> 3.  Marine Biological Lab definition counting each gap opening (internal or terminal) as a single mismatch, whether or not the gap was extended: 1.0 - [(mismatches + gap openings)/(longest sequence length)]
>
>  4.  BLAST definition, equivalent to --iddef 1 in a context of global pairwise alignment.
* `vsearch_query_cov`: minimum coverage for query sequence, default value is 1 (100%). More details from vsearch docummentation:
> Reject if the fraction of the query aligned to the target sequence is lower than real (value ranging from 0.0 to 1.0 included). The query coverage is  computed  as  (matches  +  mis‐matches) / query sequence length. Internal or terminal gaps are not taken into account.

### IDTAXA parameters

* `skip_idtaxa`: set to `true` (default) if you want to skip taxonomic assignment with idtaxa. If `false`, `idtaxa_refdb` have to be specified
* `idtaxa_refdb`: path to IDTAXA formated reference database
* `idtaxa_threshold`: `DECIPHER::IdTaxa()` threshold argument (default: 60):
> Numeric specifying the confidence at which to truncate the output taxonomic classifications.  Lower values of ‘threshold’ will classify deeper into the taxonomic tree at the expense of accuracy, and vise-versa for higher values of ‘threshold’. 
* `idtaxa_minDescend`: `DECIPHER::IdTaxa()` minDescend argument (default: 1):
> Numeric giving the minimum fraction of ‘bootstraps’ required to descend the tree during the initial tree descend phase of the algorithm.  Higher values are less likely to descend the tree, causing direct comparison against more sequences in the ‘trainingSet’.  Lower values may increase classification speed at the expense of accuracy.  Suggested values are between ‘1.0’ and ‘0.9’.

### DADA2 `assignTaxonomy()` parameters

* `skip_dada2`: set to `true` (default) if you want to skip taxonomic assignment with dada2. If `false`, `dada2_refdb` have to be specified
* `dada2_refdb`: path to dada2 formated reference database
* `dada2_minBoot`: `dada2::assignTaxonomy()` minBoot argument:
> The minimum bootstrap confidence for assigning a taxonomic level.